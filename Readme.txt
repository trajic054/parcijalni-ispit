<VirtualHost *:80>
    DocumentRoot "/var/www/ParcijalniIspit/"
    DirectoryIndex index.html
    ServerName parcijalni-ispit.loc
    ServerAlias www.parcijalni-ispit.loc
        <Directory "/var/www/ParcijalniIspit/">
                Options All
                AllowOverride All
                Require all granted
        </Directory>
</VirtualHost>
